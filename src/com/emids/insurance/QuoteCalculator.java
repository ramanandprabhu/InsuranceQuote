package com.emids.insurance;

import java.util.List;

public class QuoteCalculator {

	public double calculateQuoteFor(Customer cust){
		int age = cust.getAge();
		String gender = cust.getGender();
		int basePremium = 5000;
		double quote = 0.0;
		
		quote = calculateIncreaseForAge(age, basePremium);
		
		quote = calculateIncrForGender(gender, basePremium, quote);
		
		
		quote = calculateIncrForHealthConditions(cust, basePremium, quote);	
		
		quote = calculateIncrForHabits(cust, basePremium, quote);
		
		return quote;
	}

	private double calculateIncrForGender(String gender, int basePremium, double quote) {
		if("Male".equals(gender)){
			quote = quote + (basePremium*0.01)*2;
		}
		return quote;
	}

	private double calculateIncrForHabits(Customer cust, int basePremium, double quote) {
		List<String> goodHabits = cust.getgoodHabits();
		List<String> badHabits = cust.getbadHabits();
		
		
		if(goodHabits.size() >0){
			quote = quote - (basePremium*0.01)*3*goodHabits.size();
		}
		
		if(badHabits.size() >0){
			quote = quote + (basePremium*0.01)*3*badHabits.size();
		}
		return quote;
	}

	private double calculateIncrForHealthConditions(Customer cust, int basePremium, double quote) {
		List<String> healthConditions = cust.getHealthConditions();
		if(healthConditions.size()>0){
			quote = quote + (basePremium*0.01)*healthConditions.size();
		}
		return quote;
	}

	private double calculateIncreaseForAge(int age, int basePremium) {
		double quote;
		if(age < 18 ){
			quote = basePremium;
		}else if(age >=18 && age <= 40 ){
			quote = basePremium +	(basePremium*0.10);
		}else{
			quote = basePremium +	(basePremium*0.10)*2;
		}
		return quote;
	}
}
