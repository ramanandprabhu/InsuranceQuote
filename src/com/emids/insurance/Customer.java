package com.emids.insurance;

import java.util.List;

public class Customer {
	
	private String name;
	private int age;
	private String gender;
	
	List<String> healthConditions;
	List<String> goodHabits;
	List<String> badHabits;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public List<String> getHealthConditions() {
		return healthConditions;
	}
	public void setHealthConditions(List<String> healthConditions) {
		this.healthConditions = healthConditions;
	}
	public List<String> getgoodHabits() {
		return goodHabits;
	}
	public void setgoodHabits(List<String> goodHabits) {
		this.goodHabits = goodHabits;
	}
	
	public List<String> getbadHabits() {
		return badHabits;
	}
	public void setbadHabits(List<String> badHabits) {
		this.badHabits = badHabits;
	}
	
	
	
	

}
