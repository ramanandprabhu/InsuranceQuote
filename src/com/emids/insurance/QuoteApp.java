package com.emids.insurance;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class QuoteApp {

	public static void main(String[] args) {
		Customer cust = new Customer();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter name ");
		String name = scan.nextLine();
		cust.setName(name);
		
		System.out.println("Enter gender ");
		String gender = scan.nextLine();
		cust.setGender(gender);
		
		System.out.println("Enter age ");
		int age = scan.nextInt();
		cust.setAge(age);
		
		List<String> conditionsList = getHealthConditions(scan);
		cust.setHealthConditions(conditionsList);
		
		List<String> habitList = getGoodHabits(scan);
		
		cust.setgoodHabits(habitList);
		
		
		
		List<String> badHabitList = getBadHabits(scan);
		cust.setbadHabits(badHabitList);
		scan.close();
		
		QuoteCalculator cal = new QuoteCalculator();
		System.out.println("Health Insurance Premium for "+cust.getName()+" :Rs."+cal.calculateQuoteFor(cust));
	}

	private static List<String> getBadHabits(Scanner scan) {
		String habit;
		String[] badhabits = {"Smoking","Alcohol", "Drugs", };
		System.out.println("Enter Yes/No for bad Habits");
		List<String> badHabitList = new ArrayList<String>();
		for(int i = 0; i < badhabits.length; i++){
			System.out.println(badhabits[i]);
			habit = scan.next();
			if("Yes".equals(habit))
				badHabitList.add(habit);
		}
		return badHabitList;
	}

	private static List<String> getGoodHabits(Scanner scan) {
		String[] goodhabits = {"Daily Excercise"};
		System.out.println("Enter Yes/No for good Habits");
		List<String> habitList = new ArrayList<String>();
		String habit;
		for(int i = 0; i < goodhabits.length; i++){
			System.out.println(goodhabits[i]);
			habit= scan.next();
			if("Yes".equals(habit))
				habitList.add(habit);
		}
		return habitList;
	}

	private static List<String> getHealthConditions(Scanner scan) {
		String[] conditions = {"Hypertention", "Blood Pressure", "Blood Sugar", "Overweight"};
		
		System.out.println("Enter Yes/No for health conditions");
		List<String> conditionsList = new ArrayList<String>();
		String con;
		for(int i = 0; i < conditions.length; i++){
			System.out.println(conditions[i]);
			con = scan.next();
			if("Yes".equals(con))
			conditionsList.add(con);
		}
		return conditionsList;
	}

}
